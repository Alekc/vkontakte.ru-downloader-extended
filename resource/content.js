(function(){

var init = function() {
        window.addEventListener('message', onMessage, false);
    },
    onMessage = function(event) {
        var secure = event.isTrusted
                  && event.origin.indexOf('chrome://') == 0
                  && event.source == null;

        if (!secure)
            return;

        var message = JSON.parse(event.data);
        if (message.topic == 'photo:onSwitch') {
            onPhotoSwitch();
        } else if (message.topic == 'video:showVideoviewHint') {
            showVideoviewHint();
        }
    },
    onPhotoSwitch = function() { // imports: cur, getLang()
        if (typeof cur === 'undefined')
            return;

        var downloadLink = cur.pvDownloadLink || document.getElementById('pv_download');
        if (!downloadLink)
            return;

        // restore default download link text
        downloadLink.textContent = getLang('photos_download_hq');

        var list = cur.pvData[cur.pvListId] || null;
        if (list == null)
            return;

        var photo = list[cur.pvIndex] || null;
        if (photo == null)
            return;

        var uri = null,
            hq = false;

        // for natural filename
        downloadLink.setAttribute('data-vk-title', photo.id);

        // nothing more to do if link already present
        if (downloadLink.style.display != 'none')
            return;

        var uri = ('x_src' in photo) ? photo.x_src : null;
        ['y_src', 'z_src', 'w_src'].map(function(key) {
            if (key in photo) {
                uri = photo[key];
                hq  = true;
            }
        });

        if (uri === null)
            return;

        if (hq) {
            downloadLink.textContent = getLang('photos_download_hq');
        } else {
            downloadLink.textContent = __vk__L10n['download'];
        }

        downloadLink.href = uri;
        downloadLink.style.display = 'block';
    },

    showVideoviewHint = function() { // imports: videoview.showInfo(), animate()
        // expand panel with comments and controls
        videoview.showInfo();

        var actions = document.getElementById('mv_actions'),
            container = document.getElementById('mv_layer_wrap'),
            downloadLinks = actions.getElementsByClassName('vk-downloader-link');

        if (actions && container && downloadLinks.length > 0)
            scrollUpIntoViewport(actions, container, 20, function() {
                Array.forEach(downloadLinks, highlight);
            });

        return false;
    },
    scrollUpIntoViewport = function(element, container, bottomOffset, callback) {
        var viewH = container.clientHeight,
            viewY = container.scrollTop,
            elemH = element.offsetHeight,
            elemY = getY(element, container);

        if (bottomOffset === undefined)
            bottomOffset = 0;

        // scroll if bottom of element isn’t in the viewport (with an adjustment for bottomOffset)
        var targetY = -viewH + elemY + elemH + bottomOffset;
        if (viewY < targetY)
            animate(container, {scrollTop: targetY}, 250, callback);
        else
            callback();
    },
    getY = function(obj, container) {
        if (!container)
            container = null;

        var top = 0;

        if (obj.offsetParent) {
            do {
                top += obj.offsetTop;
                if (obj == container)
                    break;
            } while (obj = obj.offsetParent);
        }

        return top;
    },
    highlight = function(element) { // imports: animate()
        animate(element, {backgroundColor: '#99ADC2'}, 250, function() {
            animate(element, {backgroundColor: '#FFF'}, 2500, function() {
                element.style.backgroundColor = '';
            });
        });
    };

init();

})();
