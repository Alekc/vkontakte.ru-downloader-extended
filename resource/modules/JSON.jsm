var EXPORTED_SYMBOLS = [];

// FF3.0 has no native JSON object, use wrapped nsIJSON instead
if (typeof JSON === 'undefined') {
    EXPORTED_SYMBOLS = ['JSON'];

    var JSON = {
      nsIJSON: Components.classes['@mozilla.org/dom/json;1'].createInstance(Components.interfaces.nsIJSON),
      parse: function(jsonString) { return this.nsIJSON.decode(jsonString) },
      stringify: function(jsObject) { return this.nsIJSON.encode(jsObject) }
    };
}
