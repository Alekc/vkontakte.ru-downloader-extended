/*
 * Copyright (c) 2012 Sergey “m17” Kolosov
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * */

const Ci = Components.interfaces;
const Cc = Components.classes;

const PREF_STRING = Ci.nsIPrefBranch.PREF_STRING;
const PREF_INT = Ci.nsIPrefBranch.PREF_INT;
const PREF_BOOL = Ci.nsIPrefBranch.PREF_BOOL;

const pref_PREFERENCE_MIGRATION_DONE = 'preferenceMigration.done';


var getPrefBranch = function(branch) {
    var prefs = Cc['@mozilla.org/preferences-service;1']
            .getService(Ci.nsIPrefService)
            .getBranch(branch);
    prefs.QueryInterface(Ci.nsIPrefBranch2);

    return prefs;
};

var getPrefForType = function(prefs, prefType, key) {
    if (prefType == PREF_STRING) {
        return prefs.getCharPref(key);
    } else if (prefType == PREF_INT) {
        return prefs.getIntPref(key);
    } else if (prefType == PREF_BOOL) {
        return prefs.getBoolPref(key);
    } else {
        return null;
    }
};

var setPrefForType = function(prefs, prefType, key, value) {
    if (prefType == PREF_STRING) {
        return prefs.setCharPref(key, value);
    } else if (prefType == PREF_INT) {
        return prefs.setIntPref(key, value);
    } else if (prefType == PREF_BOOL) {
        return prefs.setBoolPref(key, value);
    }
};

var copyPrefValue = function(sourcePrefs, targetPrefs, key) {
    var sourceType = sourcePrefs.getPrefType(key);
    var targetType = targetPrefs.getPrefType(key);
    if (sourceType === targetType) {
        var value = getPrefForType(sourcePrefs, sourceType, key);
        setPrefForType(targetPrefs, targetType, key, value);
        return true;
    } else {
        return false;
    }
};

var PreferenceMigration = function(oldBranch, newBranch) {
    var newPrefs = getPrefBranch(newBranch);
    if (newPrefs.getBoolPref(pref_PREFERENCE_MIGRATION_DONE) === true) {
        return newPrefs;
    }

    var oldPrefs = getPrefBranch(oldBranch);
    var oldPrefsChildrenCountObj = {};
    var oldPrefsChildrenList = oldPrefs.getChildList('', oldPrefsChildrenCountObj);
    if (oldPrefsChildrenCountObj.value > 0) {
        oldPrefsChildrenList.forEach(function(prefLeafName) {
            copyPrefValue(oldPrefs, newPrefs, prefLeafName);
        });
        oldPrefs.deleteBranch('');
    }

    newPrefs.setBoolPref(pref_PREFERENCE_MIGRATION_DONE, true);

    return newPrefs;
};

var EXPORTED_SYMBOLS = ['PreferenceMigration'];
