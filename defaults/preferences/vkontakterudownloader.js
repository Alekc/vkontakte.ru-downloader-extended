pref("extensions.vkontakterudownloader.audio.requestData", 'manual');
pref("extensions.vkontakterudownloader.video.requestData", true);
pref("extensions.vkontakterudownloader.downloads.naturalFileNamesForDTA", false);
pref("extensions.vkontakterudownloader.downloads.naturalFileNamesForDM", false);
pref("extensions.vkontakterudownloader.downloads.download.audio", true);
pref("extensions.vkontakterudownloader.downloads.download.video", true);
pref("extensions.vkontakterudownloader.downloads.download.photo", true);
pref("extensions.vkontakterudownloader.preferenceMigration.done", false);
