import sys
import base64
import fileinput

def main():
    for filename in sys.argv[1:]:
        with open(filename, 'rb') as f:
            print filename+":\n"+base64.encodestring(f.read())

if __name__ == '__main__':
    main()
