/*
 * Copyright (c) 2010-2012 Sergey “m17” Kolosov
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * */

(function(){

const Ci = Components.interfaces;
const Cc = Components.classes;
const Cu = Components.utils;
const Cr = Components.results;

Cu.import("resource://gre/modules/PluralForm.jsm");

try {
    Cu.import("resource://gre/modules/PrivateBrowsingUtils.jsm");
} catch (e) {}

Cu.import('resource://VKontakteRuDownloader/modules/PreferenceMigration.jsm');

const FF30 = Cc['@mozilla.org/xre/app-info;1'].getService(Ci.nsIXULAppInfo)
                                              .version.substr(0,3) == '3.0';

// FF3.0 has no native JSON object
Cu.import('resource://VKontakteRuDownloader/modules/JSON.jsm');

// FF3.0 has no String.trim method
var trim = ('trim' in String) ? 
    function(str) { return str.trim(); } :
    function(str) { return str.replace(/^\s+|\s+$/g, ''); };

var utf8_size = function(str) { return unescape(encodeURIComponent(str)).length; };

let extend = function(child, supertype) {
    child.prototype.__proto__ = supertype.prototype;
};

let defineLazyGetter = function(obj, field, callback) {
    obj.__defineGetter__(field, function() {
        delete obj[field];
        return obj[field] = callback.apply(obj);
    });
};

let Timer = function(callback, interval) {
    var nsITimer = Ci.nsITimer,
        timer = Cc['@mozilla.org/timer;1'].createInstance(nsITimer);
    timer.initWithCallback({ notify: callback }, interval, nsITimer.TYPE_REPEATING_SLACK);

    return timer;
};

// fake Underscore.js
var _ = {};

_.once = function(func) {
    var ran = false, memo;
    return function() {
        if (ran) return memo;
        ran = true;
        return memo = func.apply(this, arguments);
    };
};

// cached options
var o_naturalFileNamesForDTA = false;
var o_naturalFileNamesForDM = false;
var o_requestAudioData = 'manual';
var o_requestVideoData = true;

var VK, L10n, Audio, Video, Photo, Downloads;

VK = {
    prefs : null,
    startup : function()
    {
        this._initPreferences();

        var appcontent = document.getElementById('appcontent');
        if (appcontent)
            appcontent.addEventListener('DOMContentLoaded', this.onDOMLoad, true);
    },
    _initPreferences : function()
    {
        var oldBranch = 'vkontakterudownloader.';
        var newBranch = 'extensions.vkontakterudownloader.';

        defineLazyGetter(this, 'prefs', function() {
            return PreferenceMigration(oldBranch, newBranch);
        });
    },
    onDOMLoad : function(e)
    {
        var doc = e.originalTarget;

        if (!(doc instanceof HTMLDocument))
            return;

        try {
            if (!VK._isAllowedDomain(doc.domain))
                return;
        } catch (e if e.result == Cr.NS_ERROR_FAILURE) {
            // sometimes accessing doc.domain fails
            // these documents we ignore, obviously
            return;
        }

        L10n.init(doc);
        VK._injectCSS(doc);
        VK._injectJS(doc);

        /**
         * Window object for current tab; in the code below we do not use “doc” variable,
         * since after a while (from 30 seconds to a minute) it holds something outdated,
         * but not the document we need (garbage-collected? Gecko bug?); so we pass
         * window as an argument, and retrieve document from it when needed.
         */
        var win = doc.defaultView;

        VK._prepareWindow(win);
        VK._runParseTimer(win);

        win.addEventListener('unload', VK._onWindowUnload, true);
    },
    _isAllowedDomain : function(domain)
    {
        return (domain == 'vkontakte.ru' || domain == 'vk.com');
    },
    _injectCSS : function(doc)
    {
        var style = doc.createElement('link');
        style.setAttribute('rel', 'stylesheet');
        style.setAttribute('type', 'text/css');
        style.setAttribute('href', 'chrome://VKontakteRuDownloader/content/styles.css');
        doc.getElementsByTagName('head')[0].appendChild(style);
    },
    _injectJS : function(doc)
    {
        var sources = [
            'resource://VKontakteRuDownloader/locale/' + L10n.langCode + '.js',
            'resource://VKontakteRuDownloader/content.js'
        ];
        if (FF30)
            sources.unshift('/js/lib/json2.js');

        sources.forEach(function(src) {
            var script = doc.createElement('script');
            script.setAttribute('type', 'text/javascript');
            script.setAttribute('src', src);
            doc.getElementsByTagName('head')[0].appendChild(script);
        });
    },
    _prepareWindow : function(win)
    {
        this._bindHotkeys(win);
    },
    _bindHotkeys : function(win)
    {
        win.document.addEventListener('keypress', function(e) {
            if (e.ctrlKey && e.keyCode == 40 /* Ctrl+Down */) {
                // downloading photos
                var link = win.document.getElementById('pv_download');
                if (link)
                    Downloads.download(link);
            }
        }, true);
    },
    _runParseTimer : function(win)
    {
        // do not store timer instance in a local variable - it'll be garbage-collected
        win.__vk__timer = Timer(function() {
            if (!win.location || win.location == 'about:blank')
                return VK._killTimer(win);

            VK._updateCachedOptions();

            Audio.apply(win);
            Video.apply(win);
            Photo.injectPhotoview(win);
        }, 1000);
    },
    _updateCachedOptions: function()
    {
        o_naturalFileNamesForDTA = VK.prefs.getBoolPref('downloads.naturalFileNamesForDTA');
        o_naturalFileNamesForDM = VK.prefs.getBoolPref('downloads.naturalFileNamesForDM');
        o_requestAudioData = VK.prefs.getCharPref('audio.requestData');
        o_requestVideoData = VK.prefs.getBoolPref('video.requestData');
    },
    _killTimer : function(win)
    {
        if (win.__vk__timer) {
            win.__vk__timer.cancel();
            win.__vk__timer = null;
        }
    },
    _onWindowUnload : function()
    {
        VK._killTimer(this);
    }
};

L10n = {
    langCode: null,
    _bundle : null,
    _map : {
        '0' : 'ru-RU',
        '1' : 'uk-UA',
        '2' : 'be-BY',
        '3' : 'en-US',
        '4' : 'es-ES',
        '100' : 'ru-OLD', // “Дореволюцiонный”
        '777' : 'ru-RU',  // “В союзе” (In USSR)
        '888' : 'ru-RU'   // “Спортивный” (Sport)
    },
    init : function(doc)
    {
        /**
         * Detect language using “remixlang” cookie variable.
         * Language is Russian (ID = 0), if there's no cookie.
         * When L10n is missing, use English.
         */
        var langMatch = doc.cookie.match(/remixlang=(\d+)/);
        var langId    = langMatch ? langMatch[1] : '0';
        var langCode  = this._map[langId] || 'en-US';
        var bundle    = 'chrome://VKontakteRuDownloader/content/locale/' + langCode + '/strings.properties';

        this.langCode = langCode;
        this._bundle = Cc['@mozilla.org/intl/stringbundle;1']
                      .getService()
                      .QueryInterface(Ci.nsIStringBundleService)
                      .createBundle(bundle);

        var pluralRule = this.getString('pluralRule');
        this._getPlural = PluralForm.makeGetter(pluralRule)[0];
    },
    getString : function(name /*, params*/)
    {
        if (arguments[1])
            return this._bundle.formatStringFromName(name, arguments[1], arguments[1].length);
        else
            return this._bundle.GetStringFromName(name);
    },
    _getPlural : null,
    getPlural : function(number, name)
    {
        return this._getPlural(number, this.getString(name));
    }
};

Audio = {
    apply : function(win)
    {
        this.inject(win, 'audioRowWall', AudioItem);
        this.inject(win, 'audioRow', AudioItem);
        this.inject(win, 'audio', AudioItemEnhanced);
    },
    inject : function(win, cssClass, itemClass)
    {
        Array.forEach(
            win.document.getElementsByClassName(cssClass),
            function(node) {
                if (!node.hasAttribute('data-vk-injected'))
                    (new itemClass(node)).inject();
            }
        );
    }
};

function AudioItem(node)
{
    var m = node.id.match(/^audio(?:Wall)?(.*\d.*)$/i);
    this.id = m ? m[1] : null;

    this.document = node.ownerDocument;
    this.node = node;

    // changed as a side-effect of _getButtonNode() below
    this.newLayout = false;
    this.buttonNode = this._getButtonNode();

    // changed inside _injectDuration()
    this.oldLayoutWithActions = false;

    // cached via underscore.once()
    this._getActionsNode = _.once(this._getActionsNode);
    this._hasAnyAction = _.once(this._hasAnyAction);

    this.durationNode = null;
    this.extendedDurationNode = null;
    this.extendedDurationLinkNode = null;
    this.duration = '0:00';
    this.title = null;
    this.state = null;

    // we call instance method (_extractURI) AFTER properties above defined
    this.uri = this._extractURI();
}

AudioItem.prototype = {
    node: null,
    buttonNode: null,
    durationNode: null,
    extendedDurationNode: null,
    extendedDurationLinkNode: null,
    duration: null,
    document: null,
    newLayout: false,
    oldLayoutWithActions: false,
    title: null,
    state: null,
    uri: null,
    id: null,
    inject : function()
    {
        var node = this.node,
            uri  = this.uri;

        node.setAttribute('data-vk-injected', true);

        // if extracting URI failed, or button node not found
        // do nothing, just leave item marked as injected
        if (uri == null || this.buttonNode == null)
            return;

        this._preProcess();

        this._insertDownloadButton();

        if (o_requestAudioData != 'no') {
            this._injectDuration();
        }
    },
    _preProcess : function() {
        Util.addClass(this.buttonNode, 'vk-downloader-button-node');
    },
    _getButtonNode : function()
    {
        return this.node.getElementsByTagName('td')[0] || null;
    },
    _insertDownloadButton : function()
    {
        var href = this.uri;
        var link = this.document.createElement('a');

        if (o_naturalFileNamesForDTA)
            this._ifTitle(function() {
                link.setAttribute('title', this);
            });

        if (o_naturalFileNamesForDM)
            this._ifTitle(function() {
                href += '?/' + this.replace('/', '-') + '.mp3';
            });

        var self = this;
        link.setAttribute('href', href);
        link.setAttribute('class', 'vk-downloader-button');
        link.innerHTML = '<img src="chrome://VKontakteRuDownloader/skin/pages/download.gif" />';
        link.addEventListener('click', function(event) {
            // title used for generating human-readable filename
            self._ifTitle(function() {
                link.setAttribute('data-vk-title', this);
            });

            var downloadStarted = Downloads.download(link);
            if (downloadStarted) {
                event.stopPropagation();
                event.preventDefault();
            }

            return false;
        }, true);

        this.buttonNode.appendChild(link);
    },
    _extractURI : function()
    {
        if (this.buttonNode == null)
            return null;

        var js = this.buttonNode.innerHTML.split('onclick="')[1].split('"')[0];
        var paramStart = js.indexOf('(') + 1;
        if (paramStart == 0)
            return null;

        var params = js.substring(paramStart, js.indexOf(')')).split(',').map(trim);
        if (params.length < 2)
            return null;

        var uri;

        if (params[1].indexOf('http://') != -1) {
            uri = params[1].replace(/^'(.+)'$/, '$1');
        } else {
            params[3] = params[3].replace(/'/g,'');
            uri = 'http://cs'+params[1]+'.vkontakte.ru/u'+params[2]+'/audio/'+params[3]+'.mp3';
        }

        return uri;
    },
    _ifTitle: function(callback)
    {
        var title = this._getTitle();
        if (title)
            callback.call(title);
    },
    _getTitle : function()
    {
        var audioTitle = this.title;
        if (audioTitle === null) {
            var audioId = this.id;
            if (audioId) {
                var performer = this.document.getElementById('performer'+audioId);
                var title = this.document.getElementById('title'+audioId);

                audioTitle = this._extractAudioTitle(performer, title);
            } else {
                var titleElement = this.node.getElementsByClassName('audioTitle')[0] || null;
                if (titleElement)
                    audioTitle = trim(titleElement.textContent);
            }

            // cache title; set to false if failed to get one
            this.title = (audioTitle === null) ? false : audioTitle;
        }

        return audioTitle;
    },
    _extractAudioTitle : function(performerNode, titleNode)
    {
        var parts = [];

        if (performerNode)
            parts.push(trim(performerNode.textContent));

        if (titleNode)
            parts.push(trim(titleNode.textContent));

        return parts.join(' - ');
    },
    _injectDuration : function()
    {
        var durationNode = this.node.getElementsByClassName('duration')[0] || null;
        if (!durationNode)
            return;

        // store duration value
        this.duration = durationNode.innerHTML; // mm:ss

        this.oldLayoutWithActions = !this.newLayout && this._hasAnyAction();

        if (this.newLayout || this.oldLayoutWithActions) {
            Util.addClass(this.node, 'vk-downloader-audio-with-actions');

            // apply custom layout
            Util.removeClass(durationNode, 'duration');
            Util.addClass(durationNode, 'vk-downloader-audio-duration');

            durationNode.innerHTML = '<div class="duration"></div><span class="vk-downloader-audio-duration-ext"></span>';

            // the new duration node: select it and copy duration value there
            durationNode = this.node.getElementsByClassName('duration')[0] || null;
            durationNode.appendChild(this.document.createTextNode(this.duration));

            this.extendedDurationNode = this.node.getElementsByClassName('vk-downloader-audio-duration-ext')[0] || null;
            this.extendedDurationLinkNode = null;
        } else {
            this.extendedDurationNode = durationNode;
            this.extendedDurationLinkNode = durationNode;
        }

        this.durationNode = durationNode;

        // only request data
        if (o_requestAudioData == 'auto')
            Downloads.getContentLength(this.uri, function(){});

        if (o_requestAudioData == 'auto-render')
            this._requestAndRenderDuration();
        else // if manual or auto
            this._insertDurationLink();
    },
    _requestAndRenderDuration: function()
    {
        var self = this;
        var onLength = function(len) {
            self._renderExtendedDuration(len);
        };

        Downloads.getContentLength(this.uri, onLength);
    },
    _insertDurationLink: function()
    {
        var self = this,
            node,
            link;

        var onLinkClick = function(event) {
            link.removeEventListener('click', onLinkClick, true);
            self._onDurationClick(link);
            event.stopPropagation();
            return false;
        };

        var actionsNode = this._getActionsNode();

        /**
         * If any, it implies that the actions node is visible to user, and we can
         * use it to provide duration link by adding a new action for duration.
         * Otherwise, we add a simple link for duration (if possible).
         */
        var anyAction = this._hasAnyAction();

        var addDurationAction = function() {
            var node = self.document.createElement('div');
            node.className = 'vk-downloader-audio-duration-link-wrap';
            actionsNode.appendChild(node);

            return node;
        };

        if (this.newLayout) {
            if (actionsNode === null) {
                /**
                 * For the new layout, the actions node is only place to add link;
                 * abscence of actions node means that there's nothing more to do.
                 */
                return;
            }

            if (anyAction) {
                node = addDurationAction();
            } else {
                // add link under duration
                node = this.extendedDurationNode;
                this.durationNode.addEventListener('click', onLinkClick, true);
            }

            this.extendedDurationLinkNode = node;

            link = this.document.createElement('div');
        } else {
            if (anyAction) {
                node = addDurationAction();

                link = this.document.createElement('div');
            } else {
                node = this.extendedDurationLinkNode;

                link = this.document.createElement('a');
                link.textContent = this.duration;
            }
        }

        link.className = 'vk-downloader-audio-duration-link';
        link.setAttribute('title', L10n.getString('fileSize.get'));
        link.addEventListener('click', onLinkClick, true);

        node.textContent = '';
        node.appendChild(link);

        if (this.newLayout) {
            node.addEventListener('click', onLinkClick, true);
        }
    },
    _getActionsNode: function()
    {
        // a node containing controls for actions: [+] (add to my music), etc
        return this.node.getElementsByClassName('actions')[0] || null;
    },
    _hasAnyAction: function()
    {
        var actionsNode = this._getActionsNode();
        return !!actionsNode && actionsNode.innerHTML.indexOf('div') !== -1;
    },
    _removeDurationLink: function(link)
    {
        var removedNode;

        if (this.newLayout) {
            removedNode = this.extendedDurationLinkNode;
        } else {
            removedNode = link;
        }

        if (removedNode !== this.extendedDurationNode) {
            removedNode.parentNode.removeChild(removedNode);
        }

        this.extendedDurationLinkNode = null;
    },
    _onDurationClick: function(link)
    {
        if (this.state !== null)
            return;

        var self = this;
        var isRendered = false;
        var onLength = function(len) {
            isRendered = true;
            self.state = 'complete';
            self._removeDurationLink(link);
            self._renderExtendedDuration(len);
        };

        var isCached = Downloads.getContentLength(this.uri, onLength);
        if (!isCached && !isRendered) {
            // prevent flickering by adding class beforehand, to make it wide enough for waiting image
            Util.addClass(this.node, 'vk-downloader-audio-duration-rendered');

            this.state = 'waiting';
            link.setAttribute('title', L10n.getString('fileSize.waiting'));

            if (this.newLayout || this.oldLayoutWithActions) {
                Util.addClass(link, 'vk-downloader-audio-duration-link-waiting');
            } else {
                link.innerHTML = '<img src="/images/upload.gif" />';
            }
        }
    },
    _renderExtendedDuration: function(length)
    {
        var duration = this.duration;
        var extra = L10n.getString('fileSize.unknown');
        var bitrateClass = '';
        if (length) {
            var size = (length / 1048576.0).toFixed(1);
            var parts = duration.split(':');
            var seconds = parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
            var bitrate = ((length * 8) / seconds / 1000).toFixed(); // 1000, yep. precisely.
            extra = '' + size + ' MB (≈ ' + bitrate + ' kbps)';
            if (bitrate > 300){
                bitrateClass = ' vk-downloader-excellent'
            } else if(bitrate > 129 && bitrate < 300){
                bitrateClass += ' vk-downloader-good';
            } else {
                bitrateClass += ' vk-downloader-bad';
            }
        }

        Util.addClass(this.node, 'vk-downloader-audio-duration-rendered' + bitrateClass);

        if (this.newLayout || this.oldLayoutWithActions) {
            this.extendedDurationNode.textContent = extra;
        } else {
            this.extendedDurationNode.textContent = duration + '\u00a0/\u00a0' + extra;
        }

        // we don't need these anymore
        this.durationNode = null;
        this.extendedDurationNode = null;
        this.extendedDurationLinkNode = null;
    }
};

function AudioItemEnhanced(node)
{
    // call parent constructor
    AudioItem.call(this, node);
}

AudioItemEnhanced.prototype = {
    _preProcess: function()
    {
        var className = this.newLayout ? 'vk-downloader-audio-new' : 'vk-downloader-audio';
        this.node.className += ' ' + className;

        Util.addClass(this.buttonNode, 'vk-downloader-button-node');
    },
    _getButtonNode : function() {
        // table layout
        var buttonNode = this.node.getElementsByTagName('td')[0] || null;
        if (buttonNode) {
            return buttonNode;
        }
        // the “new” layout
        var playButtonNode = this.node.getElementsByClassName('play_btn')[0] || null;
        if (playButtonNode) {
            this.newLayout = true;

            buttonNode = this.document.createElement('div');
            buttonNode.className = 'vk-downloader-audio-button-node';
            playButtonNode.parentNode.insertBefore(buttonNode, playButtonNode.nextSibling);
        }

        return buttonNode;
    },
    _extractURI: function()
    {
        if (this.id === null)
            return null;

        var infoInput = this.document.getElementById('audio_info' + this.id);
        if (infoInput == null)
            return null;

        var params = infoInput.getAttribute('value').split(',');

        return params[0];
    },
    _getTitle: function()
    {
        var node = this.node,
            info = node.getElementsByClassName('info');
        if (info) {
            var performer = node.getElementsByTagName('b')[0] || null;
            var title = null;

            var audioId = this.id;
            if (audioId) {
                title = this.document.getElementById('title'+audioId);
            }
            if (title == null) {
                title = node.getElementsByClassName('title')[0] || null;
                if (title == null) {
                    title = node.getElementsByTagName('span')[0] || null;
                    if (title)
                        title = title.childNodes[0] || null; // TextNode, contains only title (no user info, etc)
                }
            }

            return this._extractAudioTitle(performer, title);
        }

        return false;
    }
};

extend(AudioItemEnhanced, AudioItem);

Video = {
    apply: function(window)
    {
        const document = window.document;

        var player = this._getPlayer(document);
        if (player == null)
            return;

        if (this._isVideoview(document))
            (new VideoViewer(document, player)).inject();
        else if (this._isPage(document))
            (new VideoPage(document, player)).inject();
    },
    _getPlayer: function(document)
    {
        var player = document.getElementById('video_player');
        var invalid = player == null
                   || player.tagName == 'IFRAME'; // external video service

        if (invalid)
            return null;

        return player;
    },
    _isVideoview: function(document)
    {
        var content = document.getElementById('mv_content');
        return content && content.innerHTML != '';
    },
    _isPage: function(document)
    {
        var content = document.getElementById('player_cont');
        return content != null;
    }
};

var VideoData = function(vars) {
    this.vars = vars;
    this.maxSize = 0;
    this.isVK = false;
    this.host = '';
    this.proxy = '';
    this.https = false;

    this._prepare();

    return {
        links: this.getLinks(),
        title: this.getTitle()
    };
};

VideoData.prototype = {
    vars: null,
    maxSize: 0,
    isVK: false,
    host: '',
    proxy: '',
    https: false,
    resolutions: [360, 480, 720],
    getLinks: function()
    {
        var links = { 240: this._getFLV() };
        var self = this;
        this.resolutions.forEach(function(size) {
            var uri = self._getHD(size);
            if (uri)
                links[size] = uri;
        });

        return links;
    },
    getTitle: function()
    {
        return decodeURIComponent(this.vars.md_title || '').replace(/\+/g, ' ');
    },
    _prepare: function()
    {
        var vars = this.vars;
        var toInt = function(i) { return parseInt(i, 10) || 0; };

        // host may be not a string, but an integer
        this.host = (vars.host || 'vkontakte.ru').toString();
        this.proxy = vars.proxy;
        this.https = this.proxy && toInt(vars.https) != 0;

        vars.vkid = toInt(vars.vkid);
        vars.uid  = toInt(vars.uid);

        var maxResolutionId = toInt(vars.hd);
        var maxSize = this._maxSizeFromId(maxResolutionId);

        vars.no_flv = toInt(vars.no_flv) != 0;
        vars.ip_subm = toInt(vars.ip_subm) != 0;

        this.isVK = toInt(vars.is_vk) != 0;

        vars.hd360_link = vars.hd_link || null;

        this.resolutions.forEach(function(size) {
            var link = vars['hd' + size + '_link'];
            if (link && maxSize < size)
                maxSize = size;
        });

        this.maxSize = maxSize;
    },
    _maxSizeFromId: function(id)
    {
        if (id == 0)
            return 240;

        return this.resolutions[id - 1];
    },
    _getFLV: function()
    {
        var vars = this.vars;

        if (vars.no_flv)
            return this._getHD(240);

        if (vars.sd_link)
            return vars.sd_link;

        if (vars.uid <= 0) {
            if (vars.ip_subm) {
                return this._getProtocol() + this.host + '/assets/video/' + vars.vtag + '' + vars.vkid + '.vk.flv';
            }
            return this._getProtocol() + this.host + '/assets/videos/' + vars.vtag + '' + vars.vkid + '.vk.flv';
        }

        if (vars.ip_subm) {
            return this._getHost() + 'u' + this._getUid(vars.uid) + '/videos/' + vars.vtag + '.flv';
        }

        return this._getHost() + 'u' + this._getUid(vars.uid) + '/video/' + vars.vtag + '.flv';
    },
    _getHD: function(size)
    {
        var vars = this.vars;

        if (size > 240) {
            var link = vars['hd' + size + '_link'];
            if (link)
                return link;
        }

        if (vars.uid <= 0 || size > this.maxSize)
            return null;

        if (vars.ip_subm) {
            return this._getHost() + 'u' + this._getUid(vars.uid) + '/videos/' + vars.vtag + '.' + size + '.mp4';
        }

        return this._getHost() + 'u' + this._getUid(vars.uid) + '/video/' + vars.vtag + '.' + size + '.mp4';
    },
    _getHost: function()
    {
        var host = this.host;
        if (host.substr(0, 4) == 'http')
            return host;

        if (this.proxy) {
            return this._getProtocol() + this.proxy + '.userapi.com/c' + host + '/';
        }

        return this._getProtocol() + 'cs' + host + '.' + this._baseHost();
    },
    _getProtocol: function()
    {
        return this.https ? 'https://' : 'http://';
    },
    _getUid: function(uid)
    {
        uid = '' + uid;
        while (uid.length < 5)
            uid = '0' + uid;

        return uid;
    },
    _baseHost: function()
    {
        return this.isVK ? 'vk.com/' : 'vkontakte.ru/';
    }
};

function VideoBase(document, player)
{
    this.document = document;
    this.player   = player;
    this.links    = {};
    this.title    = '';
    this.controls = null;
}

VideoBase.prototype = {
    document: null,
    player: null,
    links: null,
    title: null,
    actions: null,
    inject: function()
    {
        if (!this._getRequiredElements() || this._isInjected())
            return;

        this._markAsInjected();

        // required data
        if (!this._prepare())
            return;

        this._preProcess();

        this._insertLinks();
    },
    _getRequiredElements: function()
    {
        var actions = this.document.getElementById('mv_actions');
        if (actions == null)
            return false;

        this.actions = actions;

        return true;
    },
    _isInjected: function()
    {
        return this.actions.hasAttribute('data-vk-injected');
    },
    _markAsInjected: function()
    {
        this.actions.setAttribute('data-vk-injected', true);
    },
    _prepare: function()
    {
        var flashvars = this._getFlashVars();
        if (flashvars == null || flashvars == '')
            return false;

        var vars = Util.queryStringToObject(flashvars);
        var data = new VideoData(vars);

        this.links = data.links;
        this.title = data.title;

        return true;
    },
    _getFlashVars: function()
    {
        return this.player.getAttribute('flashvars');
    },
    _preProcess: function() {},
    _insertLinks: function()
    {
        for (var res in this.links) {
            var link = this.links[res];
            var text = res == '240' ?
                    L10n.getString('downloadVideo') :
                    L10n.getString('downloadVideoHD', [res]);

            this._appendLink(link, text);
        }
    },
    _appendLink: function(href, text)
    {
        this.actions.appendChild(this._createLink(href, text));
    },
    _createLink: function(href, text)
    {
        var link = this.document.createElement('a');

        if (o_requestVideoData)
            Downloads.getContentLength(href, function(length) {
                var size = (length / 1048576.0).toFixed(1);
                link.setAttribute('title', '' + size + ' MB');
            });

        if (o_naturalFileNamesForDM)
            href += '?/' +
                    this.title.replace('/', '-') + // otherwise part before '/' would be lost
                    href.substr(-4); // 4 characters for .ext

        link.setAttribute('href', href);
        link.setAttribute('data-vk-title', this.title);
        link.setAttribute('class', 'vk-downloader-link');
        link.textContent = text;
        link.addEventListener('click', Downloads.downloadClick, true);

        return link;
    }
};

function VideoViewer()
{
    VideoBase.apply(this, arguments);
}

VideoViewer.prototype = {
    _preProcess: function()
    {
        this._insertDownloadHint();
    },
    _insertDownloadHint: function()
    {
        var controlsLine = this.document.getElementById('mv_controls_line');
        if (controlsLine == null)
            return;

        var wrap = this.document.createElement('span');
        wrap.id = 'vk-downloader-video-hint';

        var link = this.document.createElement('a');
        link.id = 'vk-downloader-video-hint-link';
        link.setAttribute('href', '#');
        link.textContent = L10n.getString('download');

        var self = this;
        var onDownloadHintClick = function(event) {
            // remove handler and the link node itself
            this.removeEventListener('click', onDownloadHintClick, true);
            this.parentNode.removeChild(this);

            var window = self.document.defaultView;
            var message = {topic: 'video:showVideoviewHint'};
            window.postMessage(JSON.stringify(message), window.location);

            event.stopPropagation();
            event.preventDefault();

            return false;
        };
        link.addEventListener('click', onDownloadHintClick, true);

        wrap.appendChild(link);
        controlsLine.appendChild(wrap);
    }
};

extend(VideoViewer, VideoBase);

function VideoPage()
{
    VideoBase.apply(this, arguments);
}

VideoPage.prototype = {
    _getRequiredElements: function()
    {
        var actions = this.document.getElementById('videoactions');
        if (actions == null)
            return false;

        this.actions = actions;

        return true;
    },
    _getFlashVars: function()
    {
        var params = this.player.getElementsByTagName('param');
        for (var i = 0; i < params.length; i++)
            if (params[i].getAttribute('name') == 'flashvars')
                return params[i].getAttribute('value');

        return null;
    },
    _appendLink: function(href, text)
    {
        var link = this._createLink(href, text);
        var wrap = this.document.createElement('span');
        wrap.setAttribute('class', 'action_link');
        wrap.appendChild(link);

        this.actions.appendChild(wrap);
    }
};

extend(VideoPage, VideoBase);

Photo = {
    _albumCache : {},
    injectPhotoview : function(win)
    {
        const doc = win.document;

        var actionsBlock = doc.getElementById('pv_actions');
        if (actionsBlock == null)
            return;

        if (actionsBlock.getAttribute('data-vk-injected'))
            return;

        actionsBlock.setAttribute('data-vk-injected', true);

        var link = doc.getElementById('pv_download');
        if (link == null) {
            link = doc.createElement('a');
            link.id = 'pv_download';
            link.style.display = 'none';

            actionsBlock.appendChild(link);
        }

        link.setAttribute('class', 'vk-downloader-link');
        link.addEventListener('click', Downloads.downloadClick, true);

        var onPhotoSwitch = function() {
            var message = { topic: 'photo:onSwitch' };
            win.postMessage(JSON.stringify(message), win.location);
        };

        onPhotoSwitch();

        var pvPhoto = doc.getElementById('pv_photo');
        if (pvPhoto && !pvPhoto.getAttribute('data-vk-injected')) {
            pvPhoto.setAttribute('data-vk-injected', true);

            if (typeof MutationObserver !== 'undefined') {
                var observer = new MutationObserver(onPhotoSwitch);
                observer.observe(pvPhoto, {
                    attributes: true,
                    childList: true
                });
            } else {
                // support for versions without MutationObserver (below Firefox 14)
                pvPhoto.addEventListener('DOMSubtreeModified', onPhotoSwitch, true);
            }
        }
    }
};

Downloads = {
    _lengthCache: {},
    getContentLength : function(uri, onLength) // returns boolean: is data cached
    {
        var cache = this._lengthCache;

        if (uri in cache) {
            var length = cache[uri];
            onLength(length);
            return true;
        }

        var xhr = new XMLHttpRequest();
        xhr.open('HEAD', uri, true);
        xhr.addEventListener('load', function() {
            var length = parseFloat(xhr.getResponseHeader('Content-Length'));
            if (length) {
                // cache only valid response
                cache[uri] = length;
                onLength(length);
            }
        }, false);
        xhr.addEventListener('error', function() {
            onLength(null);
        }, false);
        xhr.send(null);

        return false;
    },
    downloadClick : function(event) /* called in context of link element */
    {
        var downloadStarted = Downloads.download(this);
        if (downloadStarted) {
            event.stopPropagation();
            event.preventDefault();
        }

        return false;
    },
    download : function(link)
    {
        var href = link.href;

        // we need to hit cache even if query string added to URL
        // so, drop query string, if any
        var pos = href.indexOf('?');
        if (pos != -1)
            href = href.substr(0, pos);

        var ext = href.substr(-4); // .ext
        var kindOf = {
            audio : ext == '.mp3',
            video : ext == '.flv' || ext == '.mov' || ext == '.mp4',
            photo : ext == '.jpg'
        };

        // check if we should download this kind of media
        for (var kind in kindOf) {
            var pref = 'downloads.download.' + kind;
            if (kindOf[kind] && !VK.prefs.getBoolPref(pref))
                return false;
        }

        var title = link.getAttribute('data-vk-title');
        var fileName = null;
        if (title && (kindOf.audio || kindOf.video || kindOf.photo))
            fileName = this._fixFileNameLength(title, ext);

        var filePickerTitleKey = this._getFilePickerTitleKey(kindOf);
        var window = link.ownerDocument.defaultView;

        this._saveURL(href, fileName, filePickerTitleKey, window);

        return true;
    },
    _getFilePickerTitleKey : function(kindOf)
    {
        if (kindOf.photo)
            return 'SaveImageTitle';

        // following bundle strings are missing for FF3.0
        if (FF30)
            return null;

        if (kindOf.audio)
            return 'SaveAudioTitle';
        else if (kindOf.video)
            return 'SaveVideoTitle';

        return null;
    },
    _fixFileNameLength : function(name, ext)
    {
        // filename length limit is 226 - NTFS, exFAT, ReiserFS
        name = name.substr(0, 226 - ext.length);

        // filename size limit is 255 bytes - ext2, ext3, XFS, JFS, etc
        while (utf8_size(name + ext) > 255) {
            // Since UTF-8 character size is not constant, we have to calculate
            // filename size after each removed character
            name = name.slice(0, -1);
        }

        return name + ext;
    },
    _saveURL : function(aURL, aDefaultFileName, aFilePickerTitleKey, window)
    {
        var fileInfo, sourceURI, file;

        fileInfo = new FileInfo(aDefaultFileName);
        initFileInfo(fileInfo, aURL, null, null, null, null);
        sourceURI = fileInfo.uri;

        if (aDefaultFileName) {
            fileInfo.fileName = validateFileName(aDefaultFileName);
            fileInfo.fileBaseName = getFileBaseName(fileInfo.fileName);
        }

        var fpParams = {
            fpTitleKey: aFilePickerTitleKey,
            fileInfo: fileInfo,
            contentType: null,
            saveMode: SAVEMODE_FILEONLY,
            saveAsType: kSaveAsType_Complete,
            file: file
        };

        if (!getTargetFile(fpParams, /* skip prompt */ true))
            return;

        var OS = Cc['@mozilla.org/xre/app-info;1'].getService(Ci.nsIXULRuntime).OS;
        if (OS == 'WINNT' && !this._fixPathForWindows(fpParams.file))
            if (!getTargetFile(fpParams, false /* do not skip prompt */)) // ask user for filename, if fix failed
                return;

        this._persist(sourceURI, fpParams.file);
    },
    _fixPathForWindows : function(/* nsIFile */ file)
    {
        // Windows API: MAX_PATH is 260: drive:\path(259) + NUL(1)
        // http://msdn.microsoft.com/en-us/library/aa365247.aspx
        const MAX_PATH = 259;
        const DOT_EXT = 4; // .ext length

        if (file.path.length > MAX_PATH) {
            var name = file.leafName;
            var path = file.parent.path + '\\';

            // cut, if at least one character will remain in filename
            // (frankly speaking, at least 8 should be, and “else” case shouldn't ever happen)
            var remainingNameLength = MAX_PATH - (path.length + DOT_EXT);
            if (remainingNameLength >= 1) {
                name = name.substr(0, remainingNameLength) + name.substr(-DOT_EXT);
                file.initWithPath(path + name);
            } else
                return false; // unsuccessful fix
        }

        // successful fix, or no fix at all
        return true;
    },
    _persist : function(sourceURI, targetFile, window)
    {
        var persist = makeWebBrowserPersist(); // toolkit/content/contentAreaUtils.js
        const nsIWBP = Ci.nsIWebBrowserPersist;

        persist.persistFlags = 
            nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES |
            nsIWBP.PERSIST_FLAGS_FORCE_ALLOW_COOKIES |
            nsIWBP.PERSIST_FLAGS_FROM_CACHE |
            // let WebBrowserPersist detect encoding automatically (if any)
            nsIWBP.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;

        // get URI for the target file
        var ioService = Cc['@mozilla.org/network/io-service;1'].getService(Ci.nsIIOService);
        var targetFileURI = ioService.newFileURI(targetFile);

        // handle private browsing
        var privacyContext = null,
            isPrivate = false;
        if (window) {
            if (typeof PrivateBrowsingUtils !== 'undefined'
                && PrivateBrowsingUtils.privacyContextFromWindow) {
                privacyContext = PrivateBrowsingUtils.privacyContextFromWindow(window);
                isPrivate = privacyContext.usePrivateBrowsing;
            }
        }

        // download
        var transfer = Cc['@mozilla.org/transfer;1'].createInstance(Ci.nsITransfer);
        transfer.init(sourceURI, targetFileURI, '', null, null, null, persist, isPrivate);
        persist.progressListener = new DownloadListener(window, transfer);
        persist.saveURI(sourceURI, null, null, null, null, targetFileURI, privacyContext);
    }
};

var Util = {
    hasClass: function(obj, className) {
        return obj.nodeType === 1 && ((' ' + obj.className + ' ').replace(/[\t\r\n]/g, ' ').indexOf(' ' + className + ' ') > -1);
    },
    addClass: function(obj, className) {
        if (obj && obj.nodeType === 1) {
            if (!obj.className) {
                obj.className = className;
            } else if (!Util.hasClass(obj, className)) {
                obj.className = trim(obj.className + ' ' + className);
            }
        }
    },
    removeClass: function(obj, className) {
        if (obj && obj.className) {
            var classes = ' ' + trim(obj.className) + ' ';
            var removedClass = ' ' + trim(className) + ' ';
            obj.className = trim(classes.replace(removedClass, ' '));
        }
    },
    queryStringToObject: function(str) {
        var obj = {};
        str.split('&').forEach(function(param) {
            var parts = param.split('=');
            var key = decodeURIComponent(parts[0] || '');
            if (!key)
                return;

            var value = decodeURIComponent(parts[1] || '');
            if (/^[+-]?[0-9]+$/.test(value))              // dumb integer test
                value = parseInt(value, 10);
            else if (/^[+-]?[0-9]+\.[0-9]*$/.test(value)) // dumb float test
                value = parseFloat(value);

            obj[key] = value;
        });

        return obj;
    }
};

window.addEventListener('load', function() { VK.startup() }, false);

})();
